import Map from '../assets/projects/map.png' 
import Mugalim from '../assets/projects/Mugalim.png' 
import Shop from '../assets/projects/Shop.png' 
import Culinary from '../assets/projects/Culinary.png' 
import Music from '../assets/projects/Music.png' 

export const data=[
    {
        id:1,
        name:"City problems",
        image: Map,
        gitlab:"https://gitlab.com/hursedb73/city-problems",
        live:"https://city-problems.vercel.app/",
    },
    {
        id:2,
        name:"Mugalim",
        image: Mugalim,
        gitlab:"https://gitlab.com/hursedb73/mugalim",
        live:"https://mugalim.vercel.app/",
    },
    {
        id:3,
        name:"Shop",
        image: Shop,
        gitlab:"https://gitlab.com/hursedb73/shop",
        live:"https://shop-pi-tan.vercel.app/",
    },
    {
        id:4,
        name:"Culinary",
        image: Culinary,
        gitlab:"https://gitlab.com/hursedb73/culinary",
        live:"https://culinary-ten.vercel.app/",
    },
    {
        id:5,
        name:"Music player",
        image: Music,
        gitlab:"https://gitlab.com/hursedb73/musikapp",
        live:"https://musikapp-iota.vercel.app/",
    },
]   
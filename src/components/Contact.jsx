import React, { useState } from 'react';

const Contact = () => {
  const [userData, setUserData] = useState({
    name: '',
    email: '',
    message: '',
  });

  const [errors, setErrors] = useState({
    name: '',
    email: '',
    message: '',
  });

  const postUserData = (event) => {
    const { name, value } = event.target;
    setUserData({ ...userData, [name]: value });
  };

  const validateForm = () => {
    let isValid = true;
    const newErrors = { name: '', email: '', message: '' };

    if (!userData.name.trim()) {
      isValid = false;
      newErrors.name = 'Name is required';
    }

    if (!userData.email.trim()) {
      isValid = false;
      newErrors.email = 'Email is required';
    } else if (!/\S+@\S+\.\S+/.test(userData.email)) {
      isValid = false;
      newErrors.email = 'Email is invalid';
    }

    if (!userData.message.trim()) {
      isValid = false;
      newErrors.message = 'Message is required';
    }

    setErrors(newErrors);
    return isValid;
  };

  const submitData = async (event) => {
    event.preventDefault();

    if (validateForm()) {
      const { name, email, message } = userData;

      try {
        const res = await fetch(
          'https://offers-to-cooperate-default-rtdb.firebaseio.com/userDataReacords.json',
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              name,
              email,
              message,
            }),
          }
        );

        if (res.ok) {
          setUserData({
            name: '',
            email: '',
            message: '',
          });
          alert('Success');
        } else {
          alert('Error');
        }
      } catch (error) {
        console.error('Error submitting data:', error);
        alert('Error submitting data');
      }
    }
  };

  return (
    <div name='contact' className='w-full h-screen bg-[#0a192f] flex justify-center items-center p-4'>
      <form
        method='POST'
        action='https://getform.io/f/a699a1b2-f225-434e-b317-1fbbde8e006c'
        className='flex flex-col max-w-[600px] w-full'
      >
        <div className='pb-8'>
          <p className='text-4xl font-bold inline border-b-4 border-pink-600 text-gray-300'>Contact</p>
          <p className='text-gray-300 py-4'>\\ Submit the form below or shoot me an email - vutukuri@gmail.com</p>
        </div>
        <input
          className='bg-[#ccd6f6] p-2'
          type='text'
          placeholder='Name'
          name='name'
          value={userData.name}
          onChange={postUserData}
        />
        {errors.name && <p className='text-red-500 my-2'>{errors.name}</p>}
        <input
          className='my-4 p-2 bg-[#ccd6f6]'
          type='email'
          placeholder='Email'
          name='email'
          value={userData.email}
          onChange={postUserData}
        />
        {errors.email && <p className='text-red-500 my-2'>{errors.email}</p>}
        <textarea
          className='bg-[#ccd6f6] p-2'
          name='message'
          rows='10'
          placeholder='Message'
          value={userData.message}
          onChange={postUserData}
        />
        {errors.message && <p className='text-red-500 my-2'>{errors.message}</p>}
        <button className='text-white border-2 hover:bg-pink-600 hover:border-pink-600 px-4 py-3 my-8 mx-auto flex items-center' onClick={submitData}>
          Let's Collaborate
        </button>
      </form>
    </div>
  );
};

export default Contact;

import React from 'react';
import Me from '../assets/me.jpg';

const About = () => {
  return (
    <div name='about' className='w-full min-h-screen bg-[#0a192f] text-gray-300 flex items-center justify-center'>
      <div className='max-w-[1000px] w-full p-8 md:grid md:grid-cols-2 gap-8'>
        <div className='md:text-right pb-8 md:pl-4'>
          <p className='text-4xl md:text-5xl font-bold inline border-b-4 border-pink-600'>
            About
          </p>
          <div className='text-2xl md:text-3xl font-bold mt-2 text-[#8892b0]'>
            <p>Hi, I'm Khurshed. Nice to meet you. Please take a look around.</p>
          </div>
          <p className='text-lg md:text-xl text-[#ccd6f6] mt-4'>
            I am passionate about building excellent software that improves
            the lives of those around me. I specialize in creating software
            for clients ranging from individuals and small businesses all the
            way to large enterprise corporations.
          </p>
        </div>
        <div className='flex items-center'>
          {/* Adjust the max-w-md to make the image smaller */}
          <img
            src={Me}
            alt='Description of the image'
            className='w-full h-auto max-w-xs mx-auto rounded-md' 
          />
        </div>
      </div>
    </div>
  );
};

export default About;
